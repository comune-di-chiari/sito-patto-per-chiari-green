---
title: Scuola
subtitle: Scuola
excerpt: >-
  Riqualificazione strutture per l’infanzia, già presenti sul territorio.

  Servizi di anticipo, posticipo e mensa scolastica gratuita al fine di assecondare le esigenze di gestione della prole in ragione degli impegni lavorativi.
order: 4
---
Scuola