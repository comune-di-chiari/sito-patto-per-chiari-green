---
title: Centro Storico
subtitle: Centro Storico
excerpt: Riqualificazione e ristrutturazione degli immobili commerciali e
  residenziali, attraverso una politica urbanistica ad hoc, con sgravi ed
  incentivi.
order: 2
---
Centro Storico