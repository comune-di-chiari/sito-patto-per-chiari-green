---
title: Ambiente
subtitle: Ambiente
excerpt: Vista l’accertata situazione critica delle tre matrici ambientali
  (acqua, aria, suolo) del nostro Comune, si dovranno evitare nuovi insediamenti
  di attività che possano immettere nell’ambiente inquinanti permanenti,
  pericolosi e/o cancerogeni.
order: 1
---
### Ambiente

...

![]()

### Ambiente