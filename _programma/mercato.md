---
title: Immigrazione
subtitle: Immigrazione
excerpt: L’immigrazione non deve essere vista come un problema, essendo un
  processo storico inevitabile. Non è rinviabile, ulteriormente, l’apertura di
  uno sportello immigrati che possa governare questa dinamica.
order: 5
---
Immigrazione