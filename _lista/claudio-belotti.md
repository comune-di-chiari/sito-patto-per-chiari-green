---
name: Claudio
surname: Belotti
summary: In pensione ma sempre molto attivo. Fortemente legato alle mie radici
  in cui ritrovo sentimenti e valori, vorrei dare un contributo alla crescita
  della mia città. Sono per un'amministrazione concreta, solidale e
  lungimirante. Un supplementare occhio di riguardo per iniziative musicali non
  disturberebbe.
order: 8
image: /assets/uploads/salogni-106_santino.jpg
image_alt: Claudio Belotti
cv_url: /assets/uploads/cv_belotti_claudio.pdf
casellario_giudiziario_url: /assets/uploads/belotti_claudio_casellario_giudiziale.pdf
---
