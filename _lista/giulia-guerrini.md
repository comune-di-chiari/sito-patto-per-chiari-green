---
name: Giulia
surname: Guerrini
summary: 30 anni. educatrice professionale, vorrei dare supporto alla crescita
  della città di chiari schierandomi a supporto del candidato Salogni.
order: 11
image: /assets/uploads/salogni-200_santino.jpg
image_alt: Giulia Guerrini
cv_url: /assets/uploads/cv_guerrini_giulia.pdf
casellario_giudiziario_url: /assets/uploads/guerrini_giulia_casellario_giudiziale.pdf
---
