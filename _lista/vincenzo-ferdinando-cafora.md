---
title: Vincenzo Ferdinando Cafora
name: Vincenzo Ferdinando
surname: Cafora
summary: nato a Napoli il giorno 01/06/1972. Laureato in scienze biologiche.
  Impiegato presso azienda farmaceutica. Residente a Chiari da oltre 20 anni. Da
  sempre interessato ai temi sociali e qualità dell'ambiente. Sono fortemente
  convinto della qualità delle proposte della lista civica Patto Per Chiari
  Green insieme a Salogni Sindaco.
order: 10
image: /assets/uploads/salogni-117_santino.jpg
image_alt: Vincenzo Ferdinando Cafora
cv_url: /assets/uploads/cv_cafora_vincenzo_ferdinando.pdf
casellario_giudiziario_url: /assets/uploads/cafora_vincenzo_ferdinando_casellario_giudiziale.pdf
---
