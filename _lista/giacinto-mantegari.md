---
title: Giacinto Mantegari
name: Giacinto
surname: Mantegari
summary: nato a Chiari nel 1938, pensionato con una forte passione per la fauna
  e per la storia clarense, accudisco una coppia di cigni che da tempo sono uno
  dei simboli della nostra “ roggia “ dando la possibilità ai più piccoli di
  ammirarne la loro bellezza.
order: 12
image: /assets/uploads/salogni-100_santino.jpg
image_alt: Giacinto Mantegari
cv_url: /assets/uploads/cv_mantegari_giacinto.pdf
casellario_giudiziario_url: /assets/uploads/mantegari_giacinto_casellario_giudiziale.pdf
---
