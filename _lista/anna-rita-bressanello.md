---
title: Anna Rita Bressanello
name: Anna Rita
surname: Bressanello
summary: 63 anni, infermiera professionale in pensione, sono in lista per
  appoggiare Marco Salogni perché è un giovane che mi ispira fiducia e penso che
  meriti.
order: 9
image: /assets/uploads/salogni-124_santino.jpg
image_alt: Anna Rita Bressanello
cv_url: /assets/uploads/cv_bressanello_anna_rita.pdf
casellario_giudiziario_url: /assets/uploads/bressanello_anna_rita_casellario_giudiziale.pdf
---
