---
name: Eros
surname: Lorenzi
summary: Operatore psichiatrico presso il Fatebenefratelli di Brescia.
  Attualmente sono collaboratrice presso la Camera del lavoro CGIL di Chiari. Ho
  deciso di candidarmi perché credo nel progetto di Marco Salogni, che mette al
  centro i bisogni delle persone, per una Chiari più inclusiva e solidale.
order: 3
image: /assets/uploads/salogni-93_santino.jpg
image_alt: Eros Lorenzi
cv_url: /assets/uploads/cv_lorenzi_eros.pdf
casellario_giudiziario_url: /assets/uploads/lorenzi_eros_casellario_giudiziale.pdf
---
