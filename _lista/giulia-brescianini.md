---
title: Giulia Brescianini
name: Giulia
surname: Brescianini
summary: 20 anni, nata e cresciuta a Chiari. Studio Lingue, Culture e
  Letterature Straniere Moderne presso l’Università di Bergamo. Comunicazione e
  collaborazione sono la chiave delle mie relazioni, e del mio lavoro. Scelgo di
  candidarmi per ridonare alla città di Chiari la dinamicità per cui si è sempre
  distinta.
order: 5
image: /assets/uploads/salogni-130_santino.jpg
image_alt: Giulia Brescianini
cv_url: /assets/uploads/cv_brescianini_giulia.pdf
casellario_giudiziario_url: /assets/uploads/brescianini_giulia_casellario_giudiziale.pdf
---
