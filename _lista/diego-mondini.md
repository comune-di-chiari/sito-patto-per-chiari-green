---
title: Diego Mondini
name: Diego
surname: Mondini
summary: diplomato in Ragioneria all'Einaudi di Chiari, Istruttore alla Scuola
  della Polizia di Stato di Milano, Consigliere Provinciale CSI Brescia e
  Regionale CSI Lombardia, Operation Manager in una Società Informatica. Dopo
  diverse esperienze al di fuori dalla mia amata Chiari penso che tornare con
  una sfida molto interessante senza dimenticarmi di mio papà " Remo " da sempre
  commerciante Clarense.
order: 2
image: /assets/uploads/salogni-102_santino.jpg
image_alt: Diego Mondini
cv_url: /assets/uploads/cv_mondini_diego.pdf
casellario_giudiziario_url: /assets/uploads/mondini_diego_casellario_giudiziale.pdf
---
