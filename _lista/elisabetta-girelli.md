---
title: Elisabetta Girelli detta Luisa
name: Elisabetta
surname: Girelli
summary: 76 anni, pensionata Infermiera Professionale e Assistente Sanitaria.
  Importante esperienza di volontariato locale e internazionale sia in ambito
  disagio giovanile e tossicodipendenze sia in gruppi e Cooperative. Con lo SVI
  di Brescia a sostegno di progetti in Africa (Burundi). Ho scelto di candidarmi
  perché credo sia importante condividere le esperienze di ciascuno/a di noi al
  fine di proporre interventi che siano una risposta ai bisogni ed al bene
  comune.
order: 7
image: /assets/uploads/salogni-96_santino.jpg
image_alt: Elisabetta Girelli
cv_url: /assets/uploads/cv_girelli_luisa.pdf
casellario_giudiziario_url: /assets/uploads/girelli_elisabetta_casellario_giudiziale.pdf
---
