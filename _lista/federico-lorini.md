---
title: Federico Lorini
name: Federico
surname: Lorini
summary: 75 anni, laureato in Ingegneria Meccanica Impiantistica. Dopo quasi 40
  anni di impegno politico-amministrativo in Chiari, sento ancora una forte
  motivazione ad impegnarmi a fianco del giovane candidato Sindaco Salogni Marco
  per una nuova innovata Amministrazione, concretamente attiva e determinata in
  campo ambientale, più trasparente, più libera da condizionamenti con scelte
  sempre prioritariamente orientate al Pubblico Interesse.
order: 4
image: /assets/uploads/salogni-110_santino.jpg
image_alt: Federico Lorini
cv_url: /assets/uploads/cv_lorini_federico.pdf
casellario_giudiziario_url: /assets/uploads/lorini_federico_casellario_giudiziale.pdf
---
