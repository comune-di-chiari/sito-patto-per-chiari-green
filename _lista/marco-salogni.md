---
name: Marco
surname: Salogni
summary: Nato a Chiari il 07/02/1989 Ingegnere Civile libero professionista -
  Presidente Amministratore Delegato di Chiari Servizi S.r.l. fino al 24/06/2022
order: 13
image: /assets/uploads/salogni.jpg
image_alt: Marco Salogni
cv_url: /assets/uploads/cv_salogni_marco.pdf
casellario_giudiziario_url: /assets/uploads/salogni_marco_casellario_giudiziale.pdf
---
