---
name: Claudia
surname: Sbaraini
summary: nata e cresciuta a Chiari, sposata e madre di due figli. Laureata in
  Giurisprudenza presso la facoltà di legge di Brescia. Mi candido perché credo
  fermamente e fortemente in una politica che va incontro alle persone a viso
  aperto, decisa, trasparente e attenta, tanto nelle scelte che nel
  l’informazione quanto all’ascolto dei cittadini.
order: 1
image: /assets/uploads/salogni-121_santino.jpg
image_alt: Claudia Sbaraini
cv_url: /assets/uploads/cv_sbaraini_claudia.pdf
casellario_giudiziario_url: /assets/uploads/sbaraini_claudia_casellario_giudiziale.pdf
---
