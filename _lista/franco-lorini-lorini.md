---
title: Franco Lorini detto Fao
name: Franco
surname: Lorini
summary: oggi pensionato dopo aver dedicato una vita all’attività di artigiano
  nella mia azienda. Attualmente Consigliere Comunale e per questo intendo
  proseguire la mia attività appoggiando Salogni. Impegnato nel volontariato
  soprattutto verso la terza età e la solitudine.
order: 6
image: /assets/uploads/salogni-114_santino.jpg
image_alt: Franco Lorini
cv_url: /assets/uploads/cv_lorini_franco.pdf
casellario_giudiziario_url: /assets/uploads/lorini_franco_casellario_giudiziale.pdf
---
