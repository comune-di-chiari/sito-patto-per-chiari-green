---
layout: post
title: tour per la CITTA' 13e15 maggio
date: 2024-05-12T20:10:42.756Z
featured_image: /assets/uploads/tour_quartieri_2024-05-13e15.jpg
featured_image_alt: tour "PER LA CITTÀ"
---
𝑻𝒆𝒓𝒛𝒂 𝒆 𝒒𝒖𝒂𝒓𝒕𝒂 tappa tour “PER LA CITTÀ” dove il candidato sindaco Marco Salogni e la coalizione a suo sostegno incontrano la cittadinanza per illustrare il programma per la città e per la zona di riferimento.\
\
![🗓️](https://static.xx.fbcdn.net/images/emoji.php/v9/t5c/1/16/1f5d3.png) Lunedì 13 Maggio, ore 20:45\
![📍](https://static.xx.fbcdn.net/images/emoji.php/v9/t2d/1/16/1f4cd.png)Fronte Parco Einstein\
\
![🗓️](https://static.xx.fbcdn.net/images/emoji.php/v9/t5c/1/16/1f5d3.png) Mercoledì 15 Maggio, ore 20:30\
![📍](https://static.xx.fbcdn.net/images/emoji.php/v9/t2d/1/16/1f4cd.png)Via Isola Verde (zona Positano)