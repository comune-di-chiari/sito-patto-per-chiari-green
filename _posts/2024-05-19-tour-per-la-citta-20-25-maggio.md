---
layout: post
title: tour per la CITTA' 20/25 maggio
date: 2024-05-19T09:21:33.676Z
featured_image: /assets/uploads/screenshot-2024-05-19-09.46.19-.jpeg
featured_image_alt: tour "PER LA CITTÀ"
---
nuove tappe tour “PER LA CITTÀ” dove il candidato sindaco Marco Salogni e la coalizione a suo sostegno incontrano la cittadinanza per illustrare il programma per la città e per la zona di riferimento.\
\
![🗓️](https://static.xx.fbcdn.net/images/emoji.php/v9/t5c/1/16/1f5d3.png) Lunedì 20 Maggio, ore 20:45\
![📍](https://static.xx.fbcdn.net/images/emoji.php/v9/t2d/1/16/1f4cd.png) Parcheggio Centro Sportivo\
\
![🗓️](https://static.xx.fbcdn.net/images/emoji.php/v9/t5c/1/16/1f5d3.png) Giovedì 23 Maggio, ore 20:30\
![📍](https://static.xx.fbcdn.net/images/emoji.php/v9/t2d/1/16/1f4cd.png) Parco Elettra\
\
![🗓️](https://static.xx.fbcdn.net/images/emoji.php/v9/t5c/1/16/1f5d3.png) Sabato 25 Maggio, ore 19:00\
![📍](https://static.xx.fbcdn.net/images/emoji.php/v9/t2d/1/16/1f4cd.png) Piazza delle Erbe