---
layout: post
title: Festa di chiusura campagna elettorale
date: 2024-06-02T20:18:34.522Z
featured_image: /assets/uploads/chiusura_2024-06-06.jpg
featured_image_alt: Festa di chiusura campagna elettorale
---
Vi aspettiamo in Piazza Zanardelli ore 20:30 per la Festa di chiusura della campagna elettorale

a seguire intrattenimento musicale / rinfresco