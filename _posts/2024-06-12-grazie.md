---
layout: post
title: Grazie
date: 2024-06-12T21:25:18.244Z
featured_image: /assets/uploads/instagram.jpg
featured_image_alt: Grazie
---
PATTO PER CHIARI GREEN ringrazia Marco Salogni per aver accompagnato e guidato la coalizione in questa campagna elettorale.

Ringraziamo Marco per aver dimostrato che si possono ottenere ottimi risultati - 2.727 voti - anche nell’assoluto rispetto altrui ed in piena lealtà.

Un risultato eccellente che premia un lungo e costante lavoro.

Ringraziamo tutte le elettrici e gli elettori che hanno sostenuto Patto Per Chiari Green e che speriamo possano sostenerci anche nei prossimi anni.

Grazie !!!