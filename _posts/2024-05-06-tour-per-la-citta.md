---
layout: post
title: tour per la CITTA' 9e12 maggio
date: 2024-05-06T18:33:43.243Z
featured_image: /assets/uploads/tour_quartieri_2024-05-9e12.jpg
featured_image_alt: tour "PER LA CITTÀ"
---
Prime due tappe del tour "PER LA CITTÀ" dove il candidato sindaco Marco Salogni e la coalizione a suo sostegno incontrano la cittadinanza per illustrare il programma per la città e per la zona di riferimento.\
\
![🗓️](https://static.xx.fbcdn.net/images/emoji.php/v9/t5c/1/16/1f5d3.png) 9 Maggio, ore 21:00\
![📍](https://static.xx.fbcdn.net/images/emoji.php/v9/t2d/1/16/1f4cd.png)Al parco S. Angela Merici\
\
![🗓️](https://static.xx.fbcdn.net/images/emoji.php/v9/t5c/1/16/1f5d3.png) 12 Maggio, ore 9:45\
![📍](https://static.xx.fbcdn.net/images/emoji.php/v9/t2d/1/16/1f4cd.png)Fronte chiesa del Santellone