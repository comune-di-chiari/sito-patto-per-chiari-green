---
layout: post
title: tour per la CITTA' 27/31 maggio
date: 2024-05-27T10:52:17.666Z
featured_image: /assets/uploads/tour_quartieri_2024-05-27-31.jpg
featured_image_alt: nuove tappe tour “PER LA CITTÀ” dove il candidato sindaco
  Marco Salogni e la coalizione a suo sostegno incontrano la cittadinanza per
  illustrare il programma per la città e per la zona di riferimento.
---
Elezioni Comunali Chiari

* l’8 è il 9 giugno 2024 fai una “ X “ sul simbolo PATTO PER CHIARI GREEN
* dopo aver barrato il simbolo di PATTO PER CHIARI GREEN, puoi scrivere massimo 2 preferenze
* IMPORTANTISSIMO: le due preferenze devono essere un uomo ed una donna