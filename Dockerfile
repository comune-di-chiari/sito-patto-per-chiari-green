# Use an official Ruby runtime as a parent image
FROM ruby:3.1.0-slim-bullseye

# Install dependencies
RUN apt-get update -qq && apt-get install -y build-essential nodejs

# Install Jekyll and Bundler
RUN gem install jekyll bundler

# Set the working directory inside the container
WORKDIR /usr/src/app

# Copy the Gemfile and Gemfile.lock into the container at /usr/src/app/
COPY Gemfile* /usr/src/app/

# Bundle install
RUN bundle install

# Copy the main application.
COPY . /usr/src/app

# Crea un nuovo progetto. Da commentare dopo la prima build, cioè dopo che il progetto è stato creato
# RUN jekyll new . --force

# Inform Docker that the container is listening on the specified port at runtime.
EXPOSE 4000

# Run Jekyll
CMD ["bundle", "exec", "jekyll", "serve", "--host=0.0.0.0"]
